import { API_URL, TIMEOUT_SEC } from "./config.js";
import { timeout } from "./helpers.js";

export const state = [];

//  Refactor update user and project into a single function
export const updateRelations = async function (url, uploadData = undefined) {
  try {
    const fetchPro = fetch(url, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(uploadData),
    });
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    const data = await res.json();

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);

    return data;
  } catch (err) {
    throw err;
  }
};

export const findDataLinkedToParent = async function (parent, dataType) {
  try {
    const fetchPro = fetch(`${API_URL}/${dataType}?populate=*`);
    const res = await Promise.race([fetchPro, timeout(TIMEOUT_SEC)]);
    const data = await res.json();

    if (!res.ok) throw new Error(`${data.error.message} (${res.status})`);

    const allData = data.data;
    console.log(allData);
    let filteredData;
    switch (dataType) {
      case "comments":
        filteredData = allData.reduce((acc, cur) => {
          console.log(cur.attributes.feedbacks.data[0]?.id, parent);
          cur.attributes.feedbacks.data[0]?.id === parseInt(parent)
            ? acc.push(cur)
            : console.log(acc);
          console.log(acc);
          return acc;
        }, []);
        break;
      case "subcomments":
        filteredData = allData.reduce((acc, cur) => {
          cur.attributes.comments.data[0]?.id === parent.id
            ? acc.push(cur)
            : console.log(acc);
          return acc;
        }, []);
        break;
    }

    return filteredData;
  } catch (err) {
    throw err;
  }
};
