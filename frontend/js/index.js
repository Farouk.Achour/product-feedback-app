import * as model from "./model.js";
import { AJAX, AJAXmedia } from "./helpers.js";
import { API_URL } from "./config.js";

const link = window.location.href;

document
  .querySelector(".add-form")
  ?.querySelector(".button-cancel")
  ?.addEventListener("click", function () {
    let index = 0;
    link.split("").map((l, i) => {
      if (l === "/") index = i;
    });
    window.location.href = link.slice(0, index + 1) + "homepage.html";
  });

// Add Feedback to Strapi
document
  .querySelector(".add-form")
  ?.querySelector(".button-submit")
  ?.addEventListener("click", function () {
    // Handle empty form
    const errorMessage = `<p class="form-error-message">Can't be empty</p>`;
    document
      .querySelectorAll(".form-error-message")
      ?.forEach((err) => err.remove());

    const title = document.querySelector(".form-input").value;
    const category = document.querySelector(".form-select").value;
    const details = document.querySelector(".form-textarea").value;

    if (!title) {
      document
        .querySelector(".form-input")
        .insertAdjacentHTML("afterend", errorMessage);
    }

    if (!category) {
      document
        .querySelector(".form-select")
        .insertAdjacentHTML("afterend", errorMessage);
    }

    if (!details) {
      document
        .querySelector(".form-textarea")
        .insertAdjacentHTML("afterend", errorMessage);
    }

    if (title && category && details) {
      // Send Feedback data to Strapi
      const feedback = {
        data: {
          title: title,
          category: category,
          details: details,
        },
      };
      console.log("I have to send data to strapi");
      AJAX(`${API_URL}/feedbacks`, feedback);

      ///////////////////////////////////////////////////////
      // After sending data to Strapi go back to the homepage
      let index = 0;
      link.split("").map((l, i) => {
        if (l === "/") index = i;
      });
      window.location.href = link.slice(0, index + 1) + "homepage.html";
    }
  });

// change selected category
document.querySelectorAll(".dashboard__aside__category").forEach((category) => {
  category.addEventListener("click", async function () {
    document
      .querySelector(".dashboard__aside__category--active")
      .classList.remove("dashboard__aside__category--active");
    this.classList.add("dashboard__aside__category--active");
    const input = this;

    renderFeedbacks(input);
  });
});

// Render feedbacks on the homepage
const renderFeedbacks = async function (input) {
  if (document.querySelector(".dashboard__main__suggestions-list")) {
    document.querySelector(".dashboard__main__suggestions-list").innerHTML = "";

    // filter feedback to category
    const data = await AJAX(`${API_URL}/feedbacks`);
    console.log(data.data);
    let feedbackFiltered;
    if (input === undefined) {
      input = document.querySelector(".dashboard__aside__category--active");
    }

    if (input.textContent.trim() === "All") {
      feedbackFiltered = data.data;
    } else {
      feedbackFiltered = data.data.filter(
        (feedback) =>
          feedback.attributes.category ===
          document
            .querySelector(".dashboard__aside__category--active")
            .textContent.trim()
            .toLowerCase()
      );
      console.log(feedbackFiltered);
    }

    document.querySelector(
      ".dashboard__main__bar__heading__text"
    ).textContent = `${feedbackFiltered.length} Suggestions`;

    // I have to get comments related to feedback here

    feedbackFiltered.forEach((feedback) => {
      const suggestion = `
  <div class="suggestion-item" id="${feedback.id}">
    <div class="suggestion-item-upvotes-container">
      <div class="suggestion-item-upvotes">
        <svg
          data-v-30a547e3=""
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 11 7"
        >
          <path
            data-v-30a547e3=""
            d="m1.334 6 4-4 4 4"
            stroke="#4661E6"
            stroke-width="2"
          ></path>
        </svg>
        <span>${0}</span>
      </div>
    </div>
    <a class="suggestion-item-info">
      <p class="suggestion-item-title">${feedback.attributes.title}</p>
      <p class="suggestion-item-description">
        ${feedback.attributes.details}
      </p>
      <p class="suggestion-item-category">${feedback.attributes.category}</p>
    </a>
    <div class="suggestion-item-comments">
      <svg
        data-v-30a547e3=""
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 18 16"
      >
        <path
          data-v-30a547e3=""
          d="M2.62 16H1.346l.902-.91c.486-.491.79-1.13.872-1.823C1.036 11.887 0 9.89 0 7.794 0 3.928 3.52 0 9.03 0 14.87 0 18 3.615 18 7.455c0 3.866-3.164 7.478-8.97 7.478-1.016 0-2.078-.137-3.025-.388A4.705 4.705 0 0 1 2.62 16Z"
          fill="#CDD2EE"
        ></path>
      </svg>
      <span>0</span>
    </div>
  </div>
`;

      document
        .querySelector(".dashboard__main__suggestions-list")
        ?.insertAdjacentHTML("beforeend", suggestion);

      document
        .querySelectorAll(".suggestion-item-upvotes")
        .forEach((upvote) => {
          upvote.addEventListener("click", function (e) {
            e.stopImmediatePropagation();
            if (this.querySelector("span").textContent === "0")
              this.querySelector("span").textContent = "1";
            else this.querySelector("span").textContent = "0";
          });
        });

      document
        .querySelectorAll(".suggestion-item-info")
        .forEach((suggestion) => {
          suggestion.addEventListener("click", function (e) {
            e.stopImmediatePropagation();
            let index = 0;
            link.split("").map((l, i) => {
              if (l === "/") index = i;
            });
            const specificSuggestion =
              e.target.closest(".suggestion-item").outerHTML;
            localStorage.setItem(
              "suggestion",
              JSON.stringify(specificSuggestion)
            );

            window.location.href = link.slice(0, index + 1) + "details.html";
          });
        });
    });
  }
};
renderFeedbacks();

// Comment on each feedback
if (window.location.href.includes("details.html")) {
  document
    .querySelector(".top-bar")
    .insertAdjacentHTML(
      "afterend",
      JSON.parse(localStorage.getItem("suggestion"))
    );

  const curFeedback = document
    .querySelector(".suggestion-item")
    .getAttribute("id");

  const feedbackPreviousComments = await model.findDataLinkedToParent(
    curFeedback,
    "comments"
  );

  document.querySelector(
    ".comments-title"
  ).textContent = `${feedbackPreviousComments.length} Comments`;
  feedbackPreviousComments.forEach(async (comment) => {
    console.log(comment.attributes.img.data[0].attributes.url);
    console.log(`${API_URL}/upload/files`);
    const img = await fetch(
      `${API_URL}/upload/files/${comment.attributes.img.data[0].id}`
    );
    console.log(img);
    console.log(API_URL + comment.attributes.img.data[0].attributes.url);

    document.querySelector(".feedback-comments").insertAdjacentHTML(
      "beforeend",
      `
      <div class="comment-first">
        <div class="comment-info">
          <div class="img-container"><img src="http://localhost:1337${comment.attributes.img.data[0].attributes.url}" /></div>
          <div class="comment-author">
            <p class="author-name">${comment.attributes.username}</p>
          </div>
          <div class="reply-container">
            <p class="reply-btn">Reply</p>
          </div>
        </div>
        <p class="comment-text">
            ${comment.attributes.comment}
        </p>
      </div>
      `
    );
  });

  document
    .querySelector(".form-submit")
    .addEventListener("click", async function (e) {
      e.stopImmediatePropagation();
      const commentValue = document.querySelector(".form-textarea").value;
      const usernameValue = document.querySelector(".username-input").value;
      const userimgValue = document.querySelector(".img-input").files[0];
      console.log(1);
      //fileReader.readAsDataURL(userimgValue);
      //fileReader.addEventListener("load", async function () {
      //userimgValueEl = this.result;
      console.log(userimgValue);

      if (commentValue && usernameValue && userimgValue) {
        const comment = {
          comment: commentValue,
          username: usernameValue,
        };
        console.log(comment);
        const data = new FormData();
        data.append("data", JSON.stringify(comment));
        console.log(data);
        data.append("files.img", userimgValue);

        for (const pair of data.entries()) {
          console.log(`${pair[0]}, ${pair[1]}`);
        }

        const currentComment = await AJAXmedia(`${API_URL}/comments`, data);
        document.querySelector(".form-textarea").value = "";

        const curFeedback = document
          .querySelector(".suggestion-item")
          .getAttribute("id");

        const feedbackPreviousComments = await model.findDataLinkedToParent(
          curFeedback,
          "comments"
        );

        console.log(feedbackPreviousComments);

        const comments = {
          data: {
            comments: [...feedbackPreviousComments, currentComment.data],
          },
        };

        await model.updateRelations(
          `${API_URL}/feedbacks/${curFeedback}`,
          comments
        );

        window.location.reload();
      }
    });
}
